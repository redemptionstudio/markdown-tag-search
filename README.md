# Markdown Tag Searcher

```
Usage: script <[options]>

Options:
    -h   --help     Show this message
    -D   --debug    Enables debug mode
    -v   --version  Show TITLE, VERSION, AUTHOR, and DATE
    -V   --verbose  Turn on verbose mode
    -c   --config   Include a config file
    -a              Searches an alphabetical tag list.
    -p              Searches a tag list based on frequency.
    -l              List tags by frequency. Default action.
    -T   --total    Report total tags.
    -t   --set-tag  Sets the tag character variable (TAGCHAR)
                    to an arbitrary character(s) for the
                    search. Defaults to '@'.
                    NOTE: If your tag is a #, then surround it
                    in single (') or double (") quotes.
```

Requires `ripgrep-all`.
