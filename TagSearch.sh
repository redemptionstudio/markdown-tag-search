#!/bin/bash

################################
# Information
TITLE="Tag Search"
VERSION="0.0.3"
DATE="20230701"
AUTHOR="T. H. Wright"
LICENSE="GPLv3"
DESC="Generates a list of tags. The script can list them or transform them into a file-searching menu."
#
################################

usage() {
	cat <<EOF
$TITLE, by $AUTHOR, v. $VERSION.
  Usage: script <[options]>
  Options:
    -h   --help     Show this message
    -D   --debug    Enables debug mode
    -v   --version  Show TITLE, VERSION, AUTHOR, and DATE
    -V   --verbose  Turn on verbose mode
    -c   --config   Include a config file
    -a              Searches an alphabetical tag list.
    -p              Searches a tag list based on frequency.
    -l              List tags by frequency. Default action.
    -T   --total    Report total tags.
    -t   --set-tag  Sets the tag character variable (TAGCHAR)
                    to an arbitrary character(s) for the
                    search. Defaults to '@'.
                    NOTE: If your tag is a #, then surround it
                    in single (') or double (") quotes.
EOF

}

verbose() {
	[[ $VERBOSE = true ]] && return 0 || return 1
}

#UNSORTEDTAGS=($(grep -rI '@' | awk -F':' '{print $2}' | grep -v "^[^@]"))

CWD=$(pwd)
TAGCHAR='@'

getUnsorted() {
	UNSORTEDTAGS=($(rga -IoPe "$TAGCHAR\K\w+" $CWD))
}

getPopular() {
	if [ -n "$UNSORTEDTAGS" ]; then
		POPULARTAGS=($(for i in "${UNSORTEDTAGS[@]}"; do echo $i; done | sort | uniq -c | sort -rn | awk '{print $2}'))
	else
		echo "Error: No tags found."
	fi
}

getAlphabetical() {
	if [ -n "$UNSORTEDTAGS" ]; then
		ALPHABETICALTAGS=($(for i in "${UNSORTEDTAGS[@]}"; do echo $i; done | sort | uniq))
	else
		echo "Error: No tags found."
	fi
}

listTags() {
	getUnsorted
	if [ -n "$UNSORTEDTAGS" ]; then
		for i in "${UNSORTEDTAGS[@]}"; do
			echo $i
		done | sort | uniq -c | sort -rn
	else
		echo "Error: No tags found."
	fi
}

alphabetical() {
	getUnsorted
	getAlphabetical
	PS3="Please select a tag:"
	select opt in "${ALPHABETICALTAGS[@]}"; do
		printf "Searching for $opt in $CWD. Files found:\n\n"
		rga -rl $opt
		exit
	done
}

popular() {
	getUnsorted
	getPopular
	PS3="Please select a tag:"
	select opt in "${POPULARTAGS[@]}"; do
		printf "Searching for $opt in $CWD. Files found:\n\n"
		rga -rl $opt
		exit
	done

}

total() {
	getUnsorted
	if [ -n "$UNSORTEDTAGS" ]; then
		for i in "${UNSORTEDTAGS[@]}"; do
			echo $i
		done | sort | uniq | wc -l
	else
		echo "Error: No tags found."
	fi
}

# BEGIN OPTARGS
reset=true
for arg in "$@"; do
	if [ -n "$reset" ]; then
		unset reset
		set -- # this resets the "$@" array so we can rebuild it
	fi
	case "$arg" in
	--help) set -- "$@" -h ;;
	--version) set -- "$@" -V ;;
	--verbose) set -- "$@" -v ;;
	--debug) set -- "$@" -D ;;
	--config) set -- "$@" -c ;;
	--total) set -- "$@" -T ;;
	--set-tag) set -- "$@" -t ;;
	*) set -- "$@" "$arg" ;;
	esac
done
OPTSTRING=':hVvDc:t:aplT' #Available options
# First loop: set variables
while getopts "$OPTSTRING" opt; do
	case $opt in
	D)
		DEBUG=true
		echo "Debug mode enabled."
		;;
	v) VERBOSE=true ;;
	c) source $OPTARG ;;
	t)
		TAGCHAR=$2
		shift 2
		;;
	esac
done
OPTIND=1 # Reset opt index
# Second loop: determine user action
while getopts "$OPTSTRING" opt; do
	case $opt in
	h) usage && exit ;;
	V) echo "$TITLE, v. $VERSION by $AUTHOR. $DATE." && exit ;;
	a) alphabetical ;;
	p) popular ;;
	l) listTags ;;
	T) total ;;
	\?) echo "$TITLE: -$OPTARG: undefined option." >&2 && usage >&2 && exit ;;
	:) echo "$TITLE: -$OPTARG: missing argument." >&2 && usage >&2 && exit ;;
	esac
done
# Determine if no options passed.
if [ $OPTIND -eq 1 ]; then
	listTags
fi
shift $((OPTIND - 1))
